"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Utilities = exports.Environments = void 0;
const dotenv = require("dotenv");
const fs = require("fs");
class Environments {
    constructor() {
        this.DB_HOST = '';
        this.DB_PORT = '';
        this.DB_USER = '';
        this.DB_PASS = '';
        this.DB_NAME = '';
        this.SERVER_PORT = '';
    }
}
exports.Environments = Environments;
class Utilities {
    static getEnvPath() {
        return `${process.cwd()}/${process.env.NODE_ENV}.env`;
    }
    static getEnvironment() {
        return dotenv.parse(fs.readFileSync(Utilities.getEnvPath()));
    }
}
exports.Utilities = Utilities;
//# sourceMappingURL=utilities.js.map