"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const swagger_1 = require("@nestjs/swagger");
const app_module_1 = require("./app/app.module");
const utilities_1 = require("./utilities/utilities");
const task_service_1 = require("./task/task.service");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    const config = new swagger_1.DocumentBuilder()
        .setTitle('Api questions')
        .setDescription('Api для получения вопросов для вопросников')
        .setVersion('1.0')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup('sandbox', app, document);
    if (process.env.NODE_ENV === 'development' ||
        process.env.NODE_ENV === 'docker') {
        const taskService = app.get(task_service_1.TaskService);
        await taskService.updateQuestion();
    }
    await app.listen(utilities_1.Utilities.getEnvironment().SERVER_PORT);
}
bootstrap();
//# sourceMappingURL=main.js.map