import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from 'src/app/app.module';
import { Utilities } from 'src/utilities/utilities';
import { TaskService } from 'src/task/task.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Api questions')
    .setDescription('Api для получения вопросов для вопросников')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('sandbox', app, document);
  if (
    process.env.NODE_ENV === 'development' ||
    process.env.NODE_ENV === 'docker'
  ) {
    const taskService = app.get<TaskService>(TaskService);
    await taskService.updateQuestion();
  }

  await app.listen(Utilities.getEnvironment().SERVER_PORT);
}

bootstrap();
