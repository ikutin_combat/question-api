import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class CacheService {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}
  public async getOrSet(key: string, value: string) {
    const result = await this.cacheManager.get(key);
    if (result) return result;
    await this.cacheManager.set(key, value);
    return value;
  }
}
