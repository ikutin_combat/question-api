import { CacheModule as CM, Module } from '@nestjs/common';
import { CacheService } from './cache.service';

@Module({
  imports: [CM.register({ ttl: 30 * 60 })],
  controllers: [],
  providers: [CacheService],
  exports: [CacheService],
})
export class CacheModule {}
