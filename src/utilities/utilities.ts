import * as dotenv from 'dotenv';
import * as fs from 'fs';

export class Environments {
  DB_HOST = '';
  DB_PORT = '';
  DB_USER = '';
  DB_PASS = '';
  DB_NAME = '';
  SERVER_PORT = '';
}

export class Utilities {
  public static getEnvPath() {
    return `${process.cwd()}/${process.env.NODE_ENV}.env`;
  }

  public static getEnvironment() {
    return dotenv.parse(
      fs.readFileSync(Utilities.getEnvPath()),
    ) as unknown as Environments;
  }
}
