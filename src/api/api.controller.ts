import { Body, Controller, Get, Header, Param, Post } from '@nestjs/common';
import { ApiOkResponse, ApiParam, ApiTags } from '@nestjs/swagger';
import { CacheService } from 'src/cache/cache.service';
import { Question } from 'src/entity/question.enity';
import { ApiService } from './api.service';

@Controller('api')
@ApiTags('Api')
export class ApiController {
  constructor(
    private readonly apiServise: ApiService,
    private readonly cacheService: CacheService,
  ) {}

  @ApiOkResponse({
    description: 'Массив вопросов',
    type: Question,
    isArray: true,
  })
  @Get('/random/:limit')
  @Header('content-type', 'application/json')
  @ApiParam({
    type: Number,
    name: 'limit',
    description: 'Кол-во вопросов в ответе',
  })
  public async getRandomQuestions(@Param('limit') limit: number) {
    return JSON.stringify(
      await this.apiServise.getQuestionWitnOptions(
        await this.apiServise.generateArrayNrandom(limit),
      ),
    );
  }

  @Get('questions/:offset/:limit')
  @Header('content-type', 'application/json')
  @ApiParam({
    type: Number,
    name: 'offset',
    description: 'смещение вопросов',
  })
  @ApiParam({
    type: Number,
    name: 'limit',
    description: 'количесиво вопросов',
  })
  @ApiOkResponse({
    description: 'Массив вопросов',
    type: Question,
    isArray: true,
  })
  public async getQuestion(
    @Param('offset') offset: number,
    @Param('limit') limit: number,
  ) {
    return this.cacheService.getOrSet(
      'getQuestion',
      JSON.stringify(await this.apiServise.getQestion(offset, limit)),
    );
  }

  @Post('getInfo')
  //нужны типы
  async getInfo(@Body() body: any) {
    return await this.apiServise.getInfo(body);
  }
}
