import { Module } from '@nestjs/common';
import { CacheModule } from 'src/cache/cache.module';
import { ApiController } from './api.controller';
import { ApiService } from './api.service';

@Module({
  imports: [CacheModule],
  providers: [ApiService],
  controllers: [ApiController],
})
export class ApiModule {}
