import { Question } from 'src/entity/question.enity';
import { In } from 'typeorm';
import { Option } from 'src/entity/options.enity';
export class ApiService {
  public async getQuestions(ids: number[]) {
    return await Question.find({
      where: {
        n: In(ids),
      },
    });
  }
  public async getOptions(ids: number[]) {
    return await Option.find({
      where: {
        questionid: In(ids),
      },
    });
  }
  public async mergeOptions(questions: Question[], options: Option[]) {
    return questions.map((q) => ({
      ...q,
      options: options
        .filter((o) => o.questionid === q.id)
        .map((oo) => ({ ...oo, questionid: undefined, id: undefined })),
    }));
  }

  public async generateArrayNrandom(size: number) {
    function getRandomInt(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min)) + min;
    }
    const maxN = await this.getlastN();
    let randomNarray = [];
    randomNarray.length = size;
    randomNarray = randomNarray.fill(0, 0, size).map((_) => {
      while (true) {
        const r = getRandomInt(1, maxN + 1);
        if (!randomNarray.includes(r)) return r;
      }
    });
    return randomNarray;
  }

  public async getQuestionWitnOptions(ids: number[]) {
    const questions = await this.getQuestions(ids);
    const options = await this.getOptions(questions.map((q) => q.id));
    return await this.mergeOptions(questions, options);
  }
  public async getlastN() {
    let n = 0;
    try {
      n = (await Question.findOne({ order: { n: 'DESC' } })).n;
    } catch {}
    return n;
  }
  public async getQestion(offset: number, limit: number) {
    const questions = await Question.find({
      take: limit,
      skip: offset,
      order: { n: 'ASC' },
    });
    const options = await this.getOptions(questions.map((q) => q.id));
    return await this.mergeOptions(questions, options);
  }

  public async getInfo(infoDesc: any[]) {
    const result = [];
    for (const e of infoDesc) {
      if (e.type === 'question') {
        console.log(e);
        // let questionRes = await Question.find({
        //   select: ['question', 'uuid'],
        //   where: {
        //     uuid: e.ids,
        //   },
        // });
        const questionRes = await Question.createQueryBuilder('q')
          .select('question, uuid')
          .where(`uuid in (${e.ids})`)
          .getRawMany();
        console.log(questionRes, 'tyu');
        result.push({ type: 'question', questions: questionRes });
      }
    }
    return result;
  }
}
