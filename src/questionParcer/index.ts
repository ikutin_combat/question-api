import axios from 'axios';

interface JSQuestion {
  question: string;
  code: string | null;
  options: string[];
  rightAnswer: number;
  explanation: string;
}

class Parcer {
  private doc: string | null = null;
  private parts: string[] | null = null;
  private minValueAlph = 65;
  constructor(private readonly url: string) {}
  async getDoc(): Promise<string | null> {
    if (this.doc) return this.doc;
    this.doc = (await axios.get(this.url)).data;
    return this.doc;
  }

  async getParts(): Promise<string[]> {
    if (this.parts) return this.parts;
    this.parts = (await this.getDoc())?.match(
      /#{6}.+?\<\/details\>/gs,
    ) as string[];
    return this.parts;
  }

  async getCode(part: string): Promise<string | null> {
    const code = part.match(/\`\`\`javascript\n(.+?)\`\`\`/s);
    if (code)
      return code[0]
        .replace(/^\`\`\`javascript\n/, '')
        .replace(/\n\`\`\`$/, '');

    return null;
  }

  async getQuestion(part: string): Promise<string> {
    return part.match(/#{6}.+\d\.+(.+)/)?.[1].trim();
  }

  async getRightAnswer(part: string): Promise<number> {
    const answerStr = part.match(/#{4}\s\W+([A-Z])/);
    if (answerStr) return answerStr[1].charCodeAt(0) - this.minValueAlph + 1;
    return 0;
  }

  async getExplanation(part: string): Promise<string> {
    return part
      .match(/<details>.+<\/details>/s)?.[0]
      .replace(/<summary>.+<\/summary>/, '')
      .replace(/#{4}.+/, '')
      .replace(/<\/*.+>/g, '')
      .trim();
  }

  async getOptions(part: string): Promise<string[]> {
    return part.match(/(?<=[A-Z]\:\s).+/gm)!.map((e) => e.replace(/\`/g, ''));
  }

  async getResult(): Promise<JSQuestion[]> {
    const result = (await this.getParts())?.map(
      async (e) =>
        await new Promise<JSQuestion>((resolve, reject) =>
          Promise.all([
            this.getQuestion(e),
            this.getCode(e),
            this.getOptions(e),
            this.getRightAnswer(e),
            this.getExplanation(e),
          ])

            .then((e) => {
              const [question, code, options, rightAnswer, explanation] = e;
              resolve({ question, code, options, rightAnswer, explanation });
            })
            .catch((error) => reject(error)),
        ),
    );
    return await Promise.all(result);
  }
}

const url =
  'https://raw.githubusercontent.com/lydiahallie/javascript-questions/master/ru-RU/README.md';

export const questionParcer = new Parcer(url);
