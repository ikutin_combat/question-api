import { ApiProperty } from '@nestjs/swagger';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import * as uuid from 'uuid';
import { Question } from './question.enity';

interface OptionContract {
  id: number;
  question: Question;
  questionid: number;
  option: string;
}

@Entity()
export class Option extends BaseEntity implements OptionContract {
  constructor(option: string, question?: Question) {
    super();
    this.option = option;
    this.question = question;
    this.uuid = uuid.v4();
  }

  @PrimaryGeneratedColumn('increment')
  public id: number;

  @ManyToOne((type) => Question, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
    cascade: true,
  })
  @JoinColumn({ name: 'questionid', referencedColumnName: 'id' })
  public question: Question;

  @Column({ default: null })
  public questionid: number;

  @ApiProperty()
  @Column()
  public option: string;

  @ApiProperty()
  @Column({ type: 'uuid' })
  public uuid: string;
}
