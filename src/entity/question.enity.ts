import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { Option } from 'src/entity/options.enity';

@Entity()
export class Question extends BaseEntity {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'uuid' })
  public uuid: string;

  //порядковый номер
  @ApiProperty({ description: 'порядковый номер, всегда последовательный' })
  @Column({ generated: 'increment' })
  public n: number;

  @ApiProperty({ description: 'вопрос' })
  @Column()
  public question: string;

  @ApiProperty({ description: 'uuid правильного ответа' })
  @Column({ type: 'uuid' })
  public answerOption: string;

  @ApiProperty({ description: 'объяснение вопросов' })
  @Column()
  public explanation: string;

  @ApiProperty({
    description: 'код для вопроса',

    nullable: true,
  })
  @Column({ default: null })
  public code: string | null;

  @ApiProperty({ type: Option, isArray: true, description: 'варианты ответов' })
  private options: Option[];
}
