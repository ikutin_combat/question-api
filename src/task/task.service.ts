import { Injectable } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { Option } from 'src/entity/options.enity';
import { Question } from 'src/entity/question.enity';
import { questionParcer } from 'src/questionParcer';
import { Connection } from 'typeorm';
@Injectable()
export class TaskService {
  constructor(private readonly connection: Connection) {}
  async updateQuestion() {
    const conOption = Option.getRepository();

    const resultOptions = [] as Option[];
    for (const q of await questionParcer.getResult()) {
      const question = new Question();
      question.code = q.code;
      question.uuid = randomUUID();
      question.explanation = q.explanation;
      question.question = q.question;
      const preOptions = [] as Option[];
      const actualOptions = [] as Option[];
      for (const o of q.options) {
        preOptions.push(new Option(o));
      }
      question.answerOption = preOptions[q.rightAnswer - 1].uuid;
      for (const i in q.options) {
        const option = preOptions[i];
        option.question = question;
        actualOptions.push(option);
      }
      resultOptions.push(...actualOptions);
    }
    const con = this.connection;
    await Question.getRepository().delete({});
    await con
      .createQueryRunner()
      .query('ALTER SEQUENCE question_n_seq RESTART WITH 1');
    await con
      .createQueryRunner()
      .query('ALTER SEQUENCE question_id_seq RESTART WITH 1');
    await conOption.save(resultOptions);
  }
}
