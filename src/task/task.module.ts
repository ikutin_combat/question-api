import { Module } from '@nestjs/common';
import { Cron, ScheduleModule } from '@nestjs/schedule';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';

@Module({
  imports: [ScheduleModule.forRoot()],
  providers: [TaskService],
  controllers: [TaskController],
})
export class TaskModule {
  constructor(private readonly taskService: TaskService) {}
  @Cron('0 0 * * * *')
  async updateQuestion() {
    console.log('start', new Date());
    return await this.taskService.updateQuestion();
  }
}
