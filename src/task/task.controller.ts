import { TaskService } from './task.service';
import { Controller, Get } from '@nestjs/common';

@Controller('task')
export class TaskController {
  constructor(private readonly taskservice: TaskService) {}
  @Get('/run')
  public async getRandomQuestions() {
    await this.taskservice.updateQuestion();
    return 'OK';
  }
}
