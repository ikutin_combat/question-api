import { Module } from '@nestjs/common';
import { ApiModule } from 'src/api/api.module';
import { DbModule } from 'src/config/db.config';
import { TaskModule } from 'src/task/task.module';
import { AppService } from './app.service';

@Module({
  imports: [DbModule.register(), ApiModule, TaskModule],
  providers: [AppService],
})
export class AppModule {}
