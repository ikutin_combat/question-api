[Установка](#установка)  
[Запуск](#Запуск)  
[Работа с docker](#работа-с-docker)  
[Поддерживаеммые методы](#Поддерживаеммые-методы)  

# javascript-questions-api
это api позволяет получать вопросы основываясь на [javascript-questions](https://github.com/lydiahallie/javascript-questions)
## Установка
`npm i`
## Запуск
`npm run docker:db:build` - собирает образ db 
`npm run docker:db:run` - запускает контейнер с db и позволяет обращаться к ДБ по localhost:5432  
`npm run start:dev` - запускает приложение по localhost:3000

## Работа с docker
`npm run docker:db:build` - собирает образ db  
`npm run docker:db:run` - запускает контейнер с db и позволяет обращаться к ДБ по localhost:5432  
`npm run docker:compose:build` -  собирает образ который включает в себя все приложение  
`npm run docker:compose:run` -  запускает все приложение в контейнере к api можно обратиться по localhost:3000  

## Поддерживаеммые методы
после запуска приложения можно перейти на `localhost:3000/sandbox`  

**в кратце**  
`api/random/:limit`  
позволяет получить случайные вопросы  
_limit_ - максимальное кол-во вопросов  

`api/questions/:offset/:limit`  
позволяет получить вопросы  
_offset_ - смещение вопросов начиная с 0  
_limit_ - максимальное кол-во вопросов  
