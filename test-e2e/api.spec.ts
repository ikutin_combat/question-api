process.env.NODE_ENV = 'development';
process.env.test = 'true';
import { Test } from '@nestjs/testing';
import { AppModule } from 'src/app/app.module';
import * as request from 'supertest';
import * as fs from 'fs';
describe('Api testing', () => {
  let server: request.SuperTest<request.Test>;
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    const app = moduleRef.createNestApplication();
    await app.init();
    server = request(app.getHttpServer());
  });

  it('/random/:limit', async () => {
    const test = await server.get('/api/random/1').expect(200);
    expect(test.body.length).toBe(1);
  });
  it('/get/:offset/:limit', async () => {
    const test = await server.get('/api/get/1/1').expect(200);
    expect(test.body.length).toBe(1);
  });
});
